package api

import api.modelclasses._

/**
  * Created by nephtys on 10/22/16.
  */
trait CommandsAPI {

  /**
    * creates a new FiscalBill within the model and sents it to the equivalent Observable
    *
    * @param users       all users that are to be billed
    * @param currentDate the currentDate (most often system.currentTimeMillis), exclusive upper bound for purchases
    */
  def makeAFiscalBill(users: Seq[UUID[User]], currentDate: Milliseconds): Unit


  /**
    * update user with new name information
    *
    * @param user
    * @param newName
    * @param newSimplifiedName
    * @return
    */
  def changeNameOfUser(user: UUID[User], newName: String, newSimplifiedName: Option[String]): Unit


  /**
    * create a new user from nothing
    *
    * @param newName
    * @param newSimplifiedName
    * @param specialUser
    * @return
    */
  def createNewUser(newName: String, newSimplifiedName: Option[String], specialUser: Boolean): Unit


  /**
    * add a new tag to a user
    *
    * @param user
    * @param newTag
    * @return
    */
  def addTagToUser(user: UUID[User], newTag: String): Unit


  /**
    * delete a tag from a user
    *
    * @param user
    * @param tag
    * @return
    */
  def removeTagFromUser(user: UUID[User], tag: String): Unit

  /**
    * delete a user completely
    *
    * @param user
    * @return
    */
  def deleteUser(user: UUID[User]): Unit

  /**
    * create a new freetime with possible items to select from and an amount.
    * This is stored on the beneficiary to enable highest performance for makePurchase()
    *
    * @param beneficiary
    * @param allowedItems
    * @param amount
    * @param donor
    * @param message
    * @return
    */
  def addFreeItemToUser(beneficiary: UUID[User], allowedItems: IndexedSeq[UUID[Item]], amount: Int, donor:
  UUID[User], message: String): Unit


  ///THIS IS A SPECIAL CASE. HOW DO WE DEAL WITH THIS?
  ///current concept: write to donor and filter/map information in viewmodel from all users
  ///proposal: store them at some kind of manager class. like "Bar". but that are implementation details and dont belong here
  def createFFAFreeItem(allowedItems: IndexedSeq[UUID[Item]], amount: Int,
      donor: UUID[User], message: String): Unit


  /**
    * this only is needed for one item at at time anyway, right?
    *
    * @param donor
    * @param item
    * @return
    */
  def consumeOneFFA(donor: UUID[User], item: Item): Unit


  /**
    * can be packaged into one map (and should be, so the aggregate update is atomic)
    * all FreeItems still open are stored on the consumer, so iterating over them is simple and fast
    *
    * @param consumer
    * @param currentDate
    * @param itemAndAmount
    * @param tags
    * @return
    */
  def makePurchase(consumer: UUID[User], currentDate: Milliseconds, specialNotes: IndexedSeq[String],
      itemAndAmount: Map[Item, Int], tags:  Set[String]): Unit


  /**
    *
    * creates a new item from nothing with the given start name, description, and category, and price
    *
    * @param name
    * @param description
    * @param category
    * @param cost
    * @return
    */
  def createNewItem(name: String,
      description: String,
      category: Seq[String],
      cost: Cents): Unit


  /**
    * sets the cost of the item to the new value
    *
    * @param item
    * @param newCost
    * @return
    */
  def changeCostOfItem(item: UUID[Item], newCost: Cents): Unit


  /**
    * change the name of an item
    *
    * @param item
    * @param name
    * @return
    */
  def changeNameOfItem(item: UUID[Item], name: String): Unit

  /**
    * change the description of an item
    *
    * @param item
    * @param description
    * @return
    */
  def changeDescriptionOfItem(item: UUID[Item], description: String): Unit

  /**
    * change the category of an item
    *
    * @param item
    * @param category
    * @return
    */
  def changeCategoryOfItem(item: UUID[Item], category: Seq[String]): Unit


  /**
    * deletes the referenced item
    *
    * @param item
    */
  def deleteItem(item: UUID[User]): Unit
}
