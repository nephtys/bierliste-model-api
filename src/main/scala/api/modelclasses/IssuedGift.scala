package api.modelclasses

/**
  * Created by nephtys on 10/28/16.
  */
sealed trait IssuedGift {
  def message : String

  assert(message.length < 5000) //sanity check


  def allowedItems : Set[UUID[Item]]
  def totalAmount : Int
  def leftAmount : Int

  def gifter : UUID[User]
}

final case class IssuedFFA(message : String, allowedItems : Set[UUID[Item]], totalAmount : Int, leftAmount : Int, gifter : UUID[User])
  extends
  IssuedGift {
  //belongs to one single user and never multiple

}

final case class IssuedPersonalGift(message : String, allowedItems : Set[UUID[Item]], totalAmount : Int, leftAmount :
Int, gifter : UUID[User]) extends IssuedGift {

}
