package api.modelclasses

/**
  * Created by radi on 10/30/16.
  */
case class Cents(value : Int) extends AnyVal{
  def +(other : Cents) : Cents = {
    Cents(value + other.value)
  }

  def -(other : Cents) : Cents = {
    Cents(value - other.value)
  }

  def *(x : Int) : Cents = {
    Cents(value * x)
  }
}

object CentExtensions{
  implicit class IntHelper(x: Int) {
    def *(cents: Cents) : Cents = cents * x
  }
}
