package api.modelclasses

/**
  * Created by nephtys on 10/22/16.
  */
case class FiscalBill(fromIncluding : Milliseconds, toExcluding : Milliseconds, commentary : String, contents : Map[User,
  Map[Item,
  Int]]) {

}
