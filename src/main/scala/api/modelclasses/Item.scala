package api.modelclasses

/**
  * Created by nephtys on 10/22/16.
  */
case class Item(
                 uuid : UUID[Item],
                 name : String,
                 description : String,
                 category : Seq[String],
                 costsInCents : Int) {

}